{ config, pkgs, ... }:

{
  nixpkgs.config.allowUnfree = true;
  programs = {
    home-manager.enable = true;
    fish = {
      enable = true;
      plugins = [
        {
          name = "fasd";
          src = pkgs.fetchFromGitHub {
            owner = "oh-my-fish";
            repo = "plugin-fasd";
            rev = "38a5b6b6011106092009549e52249c6d6f501fba";
            sha256 = "06v37hqy5yrv5a6ssd1p3cjd9y3hnp19d3ab7dag56fs1qmgyhbs";
          };
        }
      ];
      shellAliases = {
        ls = "exa";
        e = "emacs -nw";
        sed = "sd";
        tree = "broot";
        mc = "bash .nix-profile/libexec/mc/mc-wrapper.should";
      };
    };
    vscode = {
       enable = true;
       package = pkgs.vscode;
       extensions = with pkgs.vscode-extensions; [
         bbenoist.Nix
         haskell.haskell
         justusadam.language-haskell
         # tuttieee.emacs-mcx
       ];
    };
    git = {
     enable = true;
     userName = "NAME";
     userEmail = "NAME@NAME.com";
     aliases = {
       st = "status";
     };
     extraConfig = {
        http = { sslCAinfo = "/etc/ssl/certs/ca-certificates.crt"; };
        push = { default = "matching"; };
     };
    };
    fzf = {
      enable = true;
      enableFishIntegration = true;
    };
    chromium = {
     enable = true;
     extensions = [
       # 4chan X the most viewable way to browse 4chan
       "ohnjgmpcibpbafdlkimncjhflgedgpam"
       # Hacker news enhancement suite: slightly fixes that never updating orange website
       "bappiabcodbpphnojdiaddhnilfnjmpm"
       # reddit enhancement suite: works best with old.reddit
       "kbmfpngjjgdllneeigpgjifpgocmfgmb"
       # reddit redirect: go to the better version of reddit old.reddit
       "dneaehbmnbhcippjikoajpoabadpodje"
       # ublock origin: is faster on firefox but still really great
       "cjpalhdlnbpafiamejdnhcphjbkeiagm"
       # do not see cookie boxes ever again
       "fihnjjcciajhdojfnbdddfaoknhalnja"
       # darkreader: fix awful website design
       "eimadpbcbfnmbkopoojfekhnkhdbieeh"
     ];
    };
    kitty = {
      enable = true;
      font.package = pkgs.ibm-plex;
      font.name = "IBM Plex Mono";
      font.size = 14;
      extraConfig = builtins.readFile ./kitty.conf;
      settings = {
        window_padding_width = 0;
        enable_audio_bell = false;
      };
    };
  };
  home = {
    enableNixpkgsReleaseCheck = false;
    username = "jane";
    homeDirectory = "/home/jane";
    stateVersion = "21.11";
    file = {
      ".xmonad/xmonad.hs".source = ./xmonad.hs;
      ".config/zathura/zathurarc".source = ./zathurarc;
      ".config/rofi/config.rasi".source = ./config.rasi;
    };
    packages = with pkgs; [
      # desktop environment
      picom # compositor
      pywal # automatic ricing
      haskellPackages.xmobar # bar
      # tools
      feh # simple image viewer
      zathura # simple pdf reader
      mpv # simple video and audio player
      ffmpeg # simple video editing
      # apps
      nicotine-plus # download music
      clementine # audio player
      zotero # reference management solution
      deluge # one of the most featureful torrent clients, written in python and
             # plugins are in python. My demands aren't that great. So I don't
             # care about supposed sluggishnezss
      # modern unix
      # see https://github.com/ibraheemdev/modern-unix
      # eventually I want to install uutils-coreutils as well, so it is fully modern unix
      htop # better top
      bat # better cat, syntax highlighting
      exa # better ls
      delta # better diff
      dust # better du
      duf # better df
      broot # better tree
      fd # better find
      ripgrep # better faster search
      ag # better faster search
      fzf # fuzzy file search
      mcfly # better history
      choose # better awk and cut
      jq # json search, extremly fast
      sd # sed
      cheat # better man pages
      tldr # better man pages
      curlie # curl but easier
      fasd # better cd
      # clojure development
      clojure
      jdk11
      leiningen
      babashka
      # partition
      gparted
    ];
  };
  xsession = {
    windowManager = {
      i3 = {
        enable = true;
        config = {
          modifier = "Mod4";
          terminal = "kitty";
          menu = "rofi -show run";
        };
      };
    };
  };
}
